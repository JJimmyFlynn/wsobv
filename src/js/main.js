$('#countdown').countdown('07/22/2014', function(event) {
	$(this).html(event.strftime(
			'<span class="num days">%D</span> <span class="sep">:</span> ' +
			'<span class="num hours">%H</span> <span class="sep">:</span> ' +
			'<span class="num minutes">%M</span> <span class="sep">:</span> ' +
			'<span class="num seconds">%S</span>'
		));
});